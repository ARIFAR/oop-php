<?php

    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $animal = new sheep ("shawn");
    echo "name = ".$animal->name . "<br>";
    echo "legs = ".$animal->leg . "<br>";
    echo "cold_blooded = ".$animal->cold_blooded . "<br> <br>";

    $frog = new frog ("buduk");
    echo "name = ".$frog->name . "<br>";
    echo "legs = ".$frog->leg . "<br>";
    echo "cold_blooded = ".$frog->cold_blooded . "<br>";
    echo "jump =".$frog->move . "<br> <br>";

    $ape = new ape ("kera sakti");
    echo "name = ".$ape->name . "<br>";
    echo "legs = ".$ape->leg . "<br>";
    echo "cold_blooded = ".$ape->cold_blooded . "<br>";
    echo "yell =".$ape->move . "<br>";

    

?>